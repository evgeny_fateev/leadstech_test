<?php

namespace app;

use LeadGenerator\Generator;
use LeadGenerator\Lead;
use Redis;

/**
 * Class Application
 *
 * @property array $except_category
 */
class Application implements ApplicationInterface
{
    private $except_category = ['Barbershop', 'Pizza'];
    public $redis_connection;

    public function __construct()
    {
        $redis = new Redis();
        $redis->connect('redis', 6379);
        $this->redis_connection = $redis;
    }

    /**
     * Функция для генерации заявок и оптравки в очередь
     */
    public function getApplication(){
        $generator = new Generator();
        $generator->generateLeads(10000, function (Lead $lead) {
            if ($this->checkCategory($lead->categoryName)){
                $format_data = $this->getFormatData($lead);
                $this->setToQueue($format_data);
            }
        });
    }

    /**
     * Форматирование данных
     *
     * @param object $data
     * @return string
     */
    private function getFormatData(object $data) : string {
         $result = sprintf('%d | %s | %s %s', $data->id, $data->categoryName, date('Y-m-d H:i:s'), PHP_EOL);
         return $result;
    }

    /**
     * Функция проверки категории
     *
     * @param string $category
     * @return bool
     */
    private function checkCategory(string $category) : bool {
        return !in_array($category, $this->except_category);
    }

    /**
     * Функция для добавления заявки в очередь
     *
     * @param string $application_data
     */
    public function setToQueue($application_data)
    {
        try {
            $redis = $this->redis_connection;
            $redis->rpush("application", $application_data);
        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }
}