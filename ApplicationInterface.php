<?php

namespace app;

interface ApplicationInterface
{
    public function getApplication();
}