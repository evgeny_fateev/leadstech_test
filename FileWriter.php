<?php

namespace app;

/**
 * Class FileWriter
 *
 * @property string $filename
 */
class FileWriter implements FileWriterInterface
{
    private $filename = 'log.txt';

    /**
     * Функция для сохранения данных заявки
     *
     * @param string $application_data
     */
    public function saveToFile(string $application_data){
        $file = fopen($this->filename, 'a');
        fwrite($file, $application_data);
        fclose($file);
    }
}