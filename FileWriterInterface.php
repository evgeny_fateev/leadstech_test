<?php

namespace app;

interface FileWriterInterface
{
    public function saveToFile(string $application_data);
}