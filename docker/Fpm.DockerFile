FROM php:7.4-fpm


RUN apt-get update && apt-get install -y locales && \
        apt-get -yy install libicu-dev icu-devtools libpng-dev libgmp-dev libzip-dev libpq-dev libxml2-dev && \
        docker-php-ext-install pdo pdo_mysql intl gmp zip xml

RUN apt-get update && apt-get install -y unzip
RUN apt-get install -y git

RUN dpkg-reconfigure locales \
        && locale-gen C.UTF-8 \
        && /usr/sbin/update-locale LANG=C.UTF-8

RUN echo 'ru_RU.UTF-8 UTF-8' >> /etc/locale.gen \
        && locale-gen

ENV ru_RU C.UTF-8
ENV LANG ru_RU.UTF-8
ENV LANGUAGE ru_RU.UTF-8


RUN pecl install -o -f redis \
    &&  rm -rf /tmp/pear \
    &&  docker-php-ext-enable redis

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- \
        --filename=composer \
        --install-dir=/usr/local/bin
COPY --chown=www-data:www-data . /var/www/html

RUN /usr/local/bin/composer install --prefer-dist

RUN echo 'catch_workers_output = yes' >> /usr/local/etc/php-fpm.d/www.conf && \
        echo 'php_admin_flag[log_errors] = on' >> /usr/local/etc/php-fpm.d/www.conf

#Add custom php settings
ARG PHP_MEMORY_LIMIT=128M;
ARG PHP_MAX_EXECUTION_TIME=60;
ARG PHP_POST_MAX_SIZE=8M;
ARG PHP_UPLOAD_MAX_FILESIZE=7M;

RUN touch /usr/local/etc/php/conf.d/custom_php.ini \
    && echo "memory_limit = ${PHP_MEMORY_LIMIT};" >> /usr/local/etc/php/conf.d/custom_php.ini \
    && echo "max_execution_time = ${PHP_MAX_EXECUTION_TIME};" >> /usr/local/etc/php/conf.d/custom_php.ini \
    && echo "post_max_size = ${PHP_POST_MAX_SIZE};" >> /usr/local/etc/php/conf.d/custom_php.ini \
    && echo "upload_max_filesize = ${PHP_UPLOAD_MAX_FILESIZE};" >> /usr/local/etc/php/conf.d/custom_php.ini
