<?php

include("vendor/autoload.php");

use app\FileWriter;

$redis = new Redis();
$redis->connect('redis', 6379);

/**
 * Воркер получает данные с очереди, записывает в файл и засыпает на 2 секунды
 */
while (true){
    $redis_result = $redis->blPop(['application'], 0);
    $redis_result = $redis_result[1];
    $file = new FileWriter();
    $file->saveToFile($redis_result);
    sleep(2);
}
